#!/usr/bin/env python3
import os
import tempfile


#testvalues = [i for i in range(500,10000,500)]
testvalues = [100,200,500,1000]

results = open('results.txt','w')
results.write('#NIter.\tSerial\tOMP\tACC_v1\tACC_v2\n')
for value in testvalues:
    #print(value)
    os.system('echo '+str(value)+' > temp')
    os.system('./serial.exe < temp > tempout')
    time_serial = open('tempout','r').readlines()[-1].split()[-2]
    os.system('OMP_NUM_THREADS=6 ./openmp.exe < temp > tempout')
    time_omp = open('tempout','r').readlines()[-1].split()[-2]
    os.system('./openacc_v1.exe < temp > tempout')
    time_openacc_v1 = open('tempout','r').readlines()[-1].split()[-2]    
    os.system('./openacc_v2.exe < temp > tempout')
    time_openacc_v2 = open('tempout','r').readlines()[-1].split()[-2]
    print(str(value)+' '+time_serial+' '+time_omp+' '+time_openacc_v1+' '+time_openacc_v2+'\n')
results.close()

